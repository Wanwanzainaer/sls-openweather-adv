# Serverless framework with appsync plugin || sls-OpenWeather

## Description

Create a serverless service to handle GraphQL request, when event get request will fetch OpenWeather
Api and return GraphQL Response.

## Requirement

- Serverless framework
- AWS AppSync
- Serverless-appsync-plugin
- GraphQL
- OpenWeather API

## How to Make GraphQL Query

### Query

- `getCurrentWeather()` and `getFiveDayForecast()` notice:`getHourlyForecast()` can not be used because the api is not free
- Params: cityName, countryCode, cityID, lon, lat, zip

### Example

By City Name

```
query {
	getCurrentWeather(cityName:"Thousand Oaks", countryCode:"us"){
		etc..
	}
}
```

or

```
query {
	getCurrentWeather(cityName:"Thousand Oaks"){
		etc..
	}
}
```

By City Id
List of city ID city.list.json.gz can be downloaded here http://bulk.openweathermap.org/sample/

```
query {
	getCurrentWeather(id:123){
		etc..
	}
}
```

By geographic coordinates

```
query {
	getCurrentWeather(lon:139.1, lat:35){
		etc..
	}
}
```

By ZIP code
Please note if country is not specified then the search works for USA as a default.

```
query {
	getCurrentWeather(zip:"91360", countryCode:"us"){
		etc..
	}
}
```

or

```
query {
	getCurrentWeather(zip:"91360"){
		etc..
	}
}
```

## GraphQL Structure

### Type CurrentWeather

```javascript
{
	coord: {
		lon: Float,
		lat: Float
	},
	weather: [{
		id: Int,
		main: String
		description: String,
		icon: String
	}],
	base: String,
	main: {
		temp: Float,
		pressure: 1010,
		humidity: 72,
		temp_min: Float,
		temp_max: Float
	},
	visibility: Int,
	wind: {
		speed: Float,
		deg: Float
	},
	clouds: {
		all: Int
	},
	dt: Int,
	sys: {
		type: Int,
		id: Int,
		message: Float,
		country: String,
		sunrise: Int,
		sunset: Int
	},
	timezone: Int,
	id: Int,
	name: String,
	cod: Int
}
```

### Type Forecast

```javascript
{
	cod: String,
	message: Float,
	cnt: Int,
	list: [
        type PerUnitForecast{
		    dt: Int,
		    type main: {
			    temp: Float,
    			temp_min: Float,
	    		temp_max: Float,
		    	pressure: Float,
			    sea_level: Float,
    			grnd_level: Float,
	    		humidity: Int,
		    	temp_kf: Int
    		},
	    	type weather: [{
		    	id: Int,
			    main: String,
    			description: String,
	    		icon: String
		    }],
    		type clouds: {
	    		all: Int
		    },
		    type wind: {
			    speed: Float,
			    deg: Float
		    },
		    type sys: {
			    pod: String
		    },
		    dt_txt: String
	    }],
    type city: {
		name: String,
		type coord: {
			lat: Float,
			lon: Float
        },
        country: String,
		timezone: Int,
		sunrise: Int,
		sunset: Int
	}
}
```

## Architecture

`Client GraphQL Request >Request template Mapping (Lambda)> AppSync Server.handler to resolver this request > Response template Mapping (Lambda) > Client get GraphQL response`

## known Mistakes

- When I want to complete this assignment ASAP, I didn't use git frequently because I thought I do not have time to figure out how to saving my AWS account information. When I am finished this project then I found a thrid part lib call ' serverless-pseudo-parameters ' (solved)

- AWS-IAM: when I try to make a new user and new group auth for AppSync, but upload fail,so I decided choice admin user to upload. (unsolved)

## Question

- In this assignment I thought I not use GraphQL benefits because I never found in this assignment I use the nest node. Do I have the better to use the GraphQL?

- About AWS Sercurity I though in this case can use COGNITO-USER-POOL is better than just use the API Key, Is it correct?

- When I try to use CI/CD with Gitlab, but the serverless framework login will pop new tab using browser to login. I had google it for a while.(Will keeping search method not finished yet)

## Assignment record

- 9/6(Firday) 9-11pm:
  Looking AWS documention (AppSync and Template-mapping) and Serverless-appsync-plugin github article and example.

- 9/7(Sat) 4pm-6pm:
  Start this assignment setting env and coding.

- 9/7(Sat) 9pm-12am:
  Upload to AWS. AWS IAM mistake is happen this period
  Success upload to AWS.

- 9/8(Sun) 7-8pm:
  Trying use Gitlab CI/CD, but find some issuse then decide writing README first.

- 9/8(Sun) 8-12pm:
  Writing README and upload to gitlab and Send API KEY and Endpoint

Total about 11 hours.
