const axios = require("axios");

module.exports = axios.create({
  baseURL: "https://api.openweathermap.org/data/2.5"
});
