"use strict";
const {
  fetchCurrentWeatherData,
  fetchHourlyForecastData,
  fetchFiveDayForecastData
} = require("./resovler/resovler");

module.exports.graphqlHandler = async (event, context, callback) => {
  switch (event.field) {
    case "getCurrentWeather": {
      const result = await fetchCurrentWeatherData(event.arguments);
      callback(null, result);
      break;
    }
    case "getHourlyForecast": {
      const result = await fetchHourlyForecastData(event.arguments);
      callback(null, result);
      break;
    }
    case "getFiveDayForecast": {
      const result = await fetchFiveDayForecastData(event.arguments);
      callback(null, result);
      break;
    }
    default: {
      callback(
        `Unknown field, unable to resolve ${event.field}`,
        null
      );
      break;
    }
  }
};
